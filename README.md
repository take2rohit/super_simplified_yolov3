# Run YoloV3 for your own custom class detection

This a very simplified format of YOLOv3 which can be used by anyone for object detection. The code returns the co-ordinate of bounding of object in the format
of top left corner and bottom right corner ```[x_top, y_top, x_bottom, y_bottom]```


### Requirements
1. Pytorch
2. OpenCV
3. python3

### How to use 
1. Clone the repository and run ```pip3 install -r requirements.txt```
2. Modify ```cam_demo.py``` by changing the given parameters of yolo_output() if required
   - Confidence (default value = 0.25)
   - Non-max supression (default value = 0.4)
3. If you want to run on custom video then ```videofile = '<path>/<to>/<video>.mp4'``` else if you want to use webcam set ```videofile = 0```
4. Navigate to directory and run ```python cam_demo.py```
5. Import this function anywhere as ``` from cam_demo import yolo_output ```

If this repository solved your object detection problem give it a Star :)